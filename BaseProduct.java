package BussinessLayer;

/**
 * Represents a product on the menu that cannot be split into multiple products
 *
 * @author Neagu Lorena
 */
public class BaseProduct extends MenuItem implements Component {

    double price;

    /**
     * Instantiates a new base product
     *
     * @param id       id
     * @param itemName name
     * @param quantity quantity
     * @param price    price
     */
    public BaseProduct(int id, String itemName, int quantity, double price) {
        super(id, itemName, quantity);
        this.price = price;
    }

    /**
     * Computes the price of the product
     *
     * @return price
     */
    @Override
    public double computePrice() {
        return price;

    }
}
