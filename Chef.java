package PresentationLayer;

import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import BussinessLayer.MenuItem;

/**
 * @author Neagu Lorena
 */
public class Chef implements Observer {

    private String chefName;
    private ArrayList<MenuItem> preparing;
    View v;
    /**
     * Creates a chef instance
     *
     * @param chefName chef name
     */
    public Chef(String chefName, ArrayList<MenuItem> mi, View v) {
        this.chefName = chefName;
        this.preparing = mi; //new ArrayList<MenuItem>();
        this.v = v;
    }

    /**
     * Chef name getter
     *
     * @return chef name
     */
    public String getChef() {
        return chefName;
    }
    /**
     * Chef preparing getter
     *
     * @return preparing list
     */
    public  ArrayList<MenuItem> getPreparing() {
        return preparing;
    }
    @Override
    public void update(Observable o, Object arg) {
    	preparing.add((MenuItem)arg);
    	v.createMenuTable(preparing);

    }
}
