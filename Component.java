package BussinessLayer;

/**
 * @author Neagu Lorena
 */
public interface Component {
    public abstract double computePrice();
}
