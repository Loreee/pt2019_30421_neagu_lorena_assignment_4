package BussinessLayer;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Represents a product on the menu that can be split into multiple products
 *
 * @author Neagu Lorena
 */
public class CompositeProduct extends MenuItem implements Component {

    ArrayList<MenuItem> items;

    /**
     * Instantiates a new composite product
     *
     * @param id       id
     * @param itemName name
     * @param quantity quantity
     * @param items    items that make up the composite item
     */
    public CompositeProduct(int id, String itemName, int quantity, ArrayList<MenuItem> items) {
        super(id, itemName, quantity);
        this.items = items;
    }

    /**
     * returns the items in this items' composition
     *
     * @return item list
     */
    public ArrayList<MenuItem> getItems() {
        return items;
    }

    /**
     * Computes the price of the item
     *
     * @return price
     */
    @Override
    public double computePrice() {
        double price = 0;

        for (Iterator<MenuItem> it = items.iterator(); it.hasNext(); ) {
            MenuItem mi = it.next();
            price += mi.computePrice();
        }
        return price;
    }

}
