package PresentationLayer;

import BussinessLayer.*;
import DataLayer.RestaurantSerializator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Defines the controller part of the GUI
 *
 * @author Neagu Lorena
 */
public class Controller {

    View view;
    Restaurant restaurant;

    /**
     * @param view       view reference
     * @param restaurant restaurant reference
     */
    public Controller(View view, Restaurant restaurant) {
        this.view = view;
        this.restaurant = restaurant;
        view.addNewMIListener(new NewMIListener());
        view.addNewCompositeMIListener(new NewCompositeMIListener());
        view.addEditListener(new EditListener());
        view.addDeleteListener(new DeleteListener());
        view.addShowListener(new ShowListener());
        view.addNewOrderListener(new NewOrderListener());
        //view.addNewCompositeOrderListener(new NewCompositeOrderListener());
        view.addShowOrderListener(new ShowOrderListener());
        view.addBillListener(new BillListener());
        view.addSaveListener(new SaveListener());
        view.addCookListener(new CookListener());

    }

    /**
     * @author Neagu Lorena
     */
    class NewMIListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            MenuItem baseProduct = new BaseProduct(Integer.parseInt(view.getTextid_mi()), view.getTextname(), Integer.parseInt(view.getTextquantity()), Double.parseDouble(view.getTextprice()));
            restaurant.createNewMenuItem(baseProduct);
        }
    }

    /**
     * @author Neagu Lorena
     */
    class NewCompositeMIListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem> baseProducts = new ArrayList<>();
            String ids[], names[], quantities[], prices[];
            ids = view.getTextid_mi().split(",");
            names = view.getTextname().split(",");
            quantities = view.getTextquantity().split(",");
            prices = view.getTextprice().split(",");
            //int i = 0;
           // for (String ns : names) {
            //	if(i == names.length) break;
              //  if (ns.equals(names[0])) {
                for(int i = 0;i <names.length - 1;i++) {
            		baseProducts.add(new BaseProduct(Integer.parseInt(ids[i + 1]), names[i + 1], Integer.parseInt(quantities[i + 1]), Double.parseDouble(prices[i])));
                    restaurant.createNewMenuItem(baseProducts.get(i));
                }
               // i++;
            
            MenuItem mi = new CompositeProduct(Integer.parseInt(ids[0]), names[0], Integer.parseInt(quantities[0]), baseProducts);
            restaurant.createNewMenuItem(mi);
        }
    }

    /**
     * @author Neagu Lorena
     */
    class NewOrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem> list = new ArrayList<>();
            MenuItem baseProduct = restaurant.getMenu().get(Integer.parseInt(view.getTextorder())); //new BaseProduct(Integer.parseInt(view.getTextid_mi()), view.getTextname(), Integer.parseInt(view.getTextquantity()), Double.parseDouble(view.getTextprice()));
            // restaurant.createNewMenuItem(baseProduct);
            list.add(baseProduct);
            restaurant.createOrder(new Order(Integer.parseInt(view.getTextid_order()), Integer.parseInt(view.getTexttable())), list);
        }
    }

    /**
     * @author Neagu Lorena
     */
   /* class NewCompositeOrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	
        	ArrayList<MenuItem> list = new ArrayList<>();
            MenuItem baseProduct = restaurant.getMenu().get(Integer.parseInt(view.getTextorder())); //new BaseProduct(Integer.parseInt(view.getTextid_mi()), view.getTextname(), Integer.parseInt(view.getTextquantity()), Double.parseDouble(view.getTextprice()));
            // restaurant.createNewMenuItem(baseProduct);
            list.add(baseProduct);
            restaurant.createOrder(new Order(Integer.parseInt(view.getTextid_order()), Integer.parseInt(view.getTexttable())), list);
        }
    }*/

    /**
     * @author Neagu Lorena
     */
    class DeleteListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            for (Iterator<MenuItem> it = restaurant.getMenu().iterator(); it.hasNext(); ) {
                MenuItem mi = it.next();
                if (mi.getItemName().equals(view.getTextdeletename())) {
                    //restaurant.deleteMenuItem(mi);
                    it.remove();
                }
            }
        }
    }

    /**
     * @author Neagu Lorena
     */
    class EditListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ArrayList<MenuItem> baseProducts = new ArrayList<>();
            String ids[], names[], quantities[], prices[];
            ids = view.getEdittextid_mi().split(",");
            names = view.getEdittextname().split(",");
            quantities = view.getEdittextquantity().split(",");
            prices = view.getEdittextprice().split(",");
            int i = 0;
            if (names.length > 1) {
                for (String ns : names) {
                    if (ns.compareTo(names[0]) != 0)
                        baseProducts.add(new BaseProduct(Integer.parseInt(ids[i]), names[i + 1], Integer.parseInt(quantities[i + 1]), Double.parseDouble(prices[i])));
                    i++;

                }
            }
            MenuItem mi;
            if (names.length > 1)
                mi = new CompositeProduct(Integer.parseInt(ids[0]), names[0], Integer.parseInt(quantities[0]), baseProducts);
            else
                mi = new BaseProduct(Integer.parseInt(view.getEdittextid_mi()), view.getEdittextname(), Integer.parseInt(view.getEdittextquantity()), Double.parseDouble(view.getEdittextprice()));

            for (Iterator<MenuItem> it = restaurant.getMenu().iterator(); it.hasNext(); ) {
                MenuItem mii = it.next();
                if (mii.getId() == Integer.parseInt(view.getEdittextid_mi())) {
                    restaurant.editMenuItem(mi);
                    break;
                }
            }
        }
    }

    /**
     * @author Neagu Lorena
     */
    class ShowListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            view.createMenuTable(restaurant.getMenu());
        }
    }

    /**
     * @author Neagu Lorena
     */
    class ShowOrderListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            view.createOrdersTable();
        }
    }

    /**
     * @author Neagu Lorena
     */
    class BillListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            for (Map.Entry<Order, ArrayList<MenuItem>> mapEntry : restaurant.getOrders().entrySet()) {
                Order o = mapEntry.getKey();
                if (o.getId() == Integer.parseInt(view.getTextbill())) {
                    restaurant.generateBill(o);
                    break;
                }
            }
        }
    }

    /**
     * @author Neagu Lorena
     */
    class SaveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            try {
                RestaurantSerializator.salvare(restaurant);
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
        }
    }
    class CookListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	ArrayList<MenuItem> mi = new ArrayList<MenuItem>();
        	ArrayList<MenuItem> itemuri = new ArrayList<MenuItem>();
        	for (Map.Entry<Order, ArrayList<MenuItem>> mapEntry : restaurant.getOrders().entrySet()) {
                Order o = mapEntry.getKey();
                mi = mapEntry.getValue();
                if(mi.get(0) instanceof CompositeProduct)
                	itemuri.add(mi.get(0));
        	}
        	Chef c = new Chef("Ricardo", itemuri, view);
        	view.createMenuTable(c.getPreparing());
        }
     }

}


