package DataLayer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author Neagu Lorena
 */
public class FileWriter {

    public FileWriter() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Redirects the output of the application to a specified file (bill.txt)
     */
    public static void redirectOutput() {
        File file = new File("bill.txt");
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        PrintStream ps = new PrintStream(fos);
        System.setOut(ps);
    }
}
