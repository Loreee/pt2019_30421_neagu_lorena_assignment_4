package BussinessLayer;

import java.io.Serializable;

/**
 * Represents an entry in the menu of the restaurant
 *
 * @author Neagu Lorena
 */
public abstract class MenuItem implements Serializable {

    private static final long serialVersionUID = 809076258106873345L;
    private int id;
    private String itemName;
    int quantity;

    /**
     * Instantiates a new menu item
     *
     * @param id       id
     * @param itemName name
     * @param quantity quantity
     */
    public MenuItem(int id, String itemName, int quantity) {
        this.id = id;
        this.itemName = itemName;
        this.quantity = quantity;
    }

    /**
     * returns the id
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * returns the name
     *
     * @return name
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * returns the quantity
     *
     * @return quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * Computes the price of the item
     *
     * @return price
     */
    public abstract double computePrice();

    @Override
    public String toString() {
        return "MenuItem [id=" + id + ", itemName=" + itemName + ", quantity=" + quantity + "]";
    }

}
