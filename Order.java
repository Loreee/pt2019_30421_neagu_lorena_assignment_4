package BussinessLayer;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

/**
 * Represents an order in the restaurant
 *
 * @author Neagu Lorena
 */
public class Order implements Serializable {

    private static final long serialVersionUID = -4435778831748207609L;
    int OrderId;
    String Date;
    int Table;

    /**
     * Instantiates a new order
     *
     * @param OrderId id
     * @param Table   table number
     */
    public Order(int OrderId, int Table) {
        this.OrderId = OrderId;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        //System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        this.Date = dateFormat.format(date);
        this.Table = Table;
    }

    /**
     * returns the id
     *
     * @return id
     */
    public int getId() {
        return OrderId;
    }

    /**
     * returns the table
     *
     * @return table
     */
    public int getTable() {
        return Table;
    }

    /**
     * returns the date when the order was placed
     *
     * @return date
     */
    public String getDate() {
        return Date;
    }

    @Override
    public int hashCode() {

        return Objects.hash(OrderId, Date, Table);
    }

    @Override
    public String toString() {
        return "Order [OrderId=" + OrderId + ", Date=" + Date + ", Table=" + Table + "]";
    }

}
