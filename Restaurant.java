package BussinessLayer;

import DataLayer.FileWriter;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Defines and implements all the needed funtionality for the restaurant
 *
 * @author Neagu Lorena
 */
public class Restaurant extends Observable implements RestaurantProcessing, Serializable {

    private static final long serialVersionUID = 8780689129918935223L;
    //Observer obs; //chef
    Map<Order, ArrayList<MenuItem>> orders;
    ArrayList<MenuItem> menu;

    /**
     * Instantiates a new restaurant
     *
     * @throws IOException deserialization failed
     */
    public Restaurant() throws IOException {
        super();
        //obs = new Chef("Ricardo");
        this.orders = new HashMap<>();
        this.menu = new ArrayList<>();
    }

    /**
     * Checks whether the restaurant was correctly defined
     *
     * @return true in case of succes, false otherwise
     */
    public boolean isWellFormed() {
        if (menu == null || orders == null) {
            return false;
        }

        int k = 0;

        for (MenuItem menuItem : menu) {
            if (!menuItem.equals(menu.get(k++))) {
                return false;
            }
        }

        if (k != menu.size()) {
            return false;
        }

        for (Map.Entry<Order, ArrayList<MenuItem>> order : orders.entrySet()) {
            if (order.getKey().getTable() < 0 || order.getValue().isEmpty() || !menu.containsAll(order.getValue())) {
                return false;
            }
        }

        return true;
    }

    /**
     * returns all the orders
     *
     * @return orders
     */
    public Map<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }

    /**
     * retruns the menu of the restaurant
     *
     * @return menu
     */
    public ArrayList<MenuItem> getMenu() {
        return menu;
    }

    @Override
    public void createNewMenuItem(MenuItem mi) {
        assert mi != null && menu != null && isWellFormed();

        int size = menu.size();

        menu.add(mi);

        assert size == menu.size() - 1;
    }

    @Override
    public void deleteMenuItem(MenuItem mi) {
        assert menu != null && mi != null && menu.size() > 0 && isWellFormed();

        int size = menu.size();

        menu.remove(mi);

        assert size == menu.size() + 1;
    }

    @Override
    public void editMenuItem(MenuItem mi) {
        assert mi != null && menu != null && menu.size() > 0 && isWellFormed();

        menu.remove(menu.get(mi.getId()));
        menu.add(mi);
    }

    @Override
    public void createOrder(Order order, ArrayList<MenuItem> list) {
        assert order != null && orders != null && list != null && list.size() > 0 && isWellFormed();

        int size = orders.size();

        //Order order = new Order(id, table);
        if (orders.containsKey(order)) {
            orders.get(order).addAll(list);
        } else {
            orders.put(order, list);
        }
        if (list.get(0) instanceof CompositeProduct) {
            setChanged();
            notifyObservers(list.get(0));
        }

        assert size == orders.size() - 1;
    }

    @Override
    public int computePrice(Order order) {
        assert orders != null && order != null && orders.size() > 0 && isWellFormed();

        int price = 0;
        ArrayList<MenuItem> list = orders.get(order);
        for (Iterator<MenuItem> it = list.iterator(); it.hasNext(); ) {
            MenuItem mi = it.next();
            price += mi.computePrice();
        }
        return price;
    }

    @Override
    public void generateBill(Order order) {
        assert orders != null && order != null && orders.size() > 0 && isWellFormed();

        double price;
        FileWriter.redirectOutput();
        System.out.println("Order number: " + order.getId());
        System.out.println("Date: " + order.getDate());
        ArrayList<MenuItem> list = orders.get(order);
        for (Iterator<MenuItem> it = list.iterator(); it.hasNext(); ) {
            MenuItem mi = it.next();
            System.out.println(mi.getItemName() + mi.getQuantity());
            price = mi.computePrice();
            System.out.println("Price : " + mi.computePrice());
        }
        System.out.println("Total: " + computePrice(order));
        System.out.println();
    }
}
