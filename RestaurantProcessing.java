package BussinessLayer;

import java.util.ArrayList;

/**
 * Defines all the operations of the restaurant
 *
 * @author Neagu Lorena
 */
public interface RestaurantProcessing {


    //Administrator: create new menu item, delete menu item, edit menu item

    /**
     * @param mi menu item to add
     * @invariant isWellFormed()
     * @pre mi != null && menu != null
     * @post getSize() = getSize()@pre + 1
     * @post @forall k : [0 .. getSize() - 1] @
     * (k < getSize() - 1 ==> getElement(k)@pre == getElement(k)) &&
     * (k == getSize() - 1 ==> getElement(k)@pre == mi)
     */
    void createNewMenuItem(MenuItem mi);

    /**
     * @param mi menu item to delete
     * @invariant isWellFormed()
     * @pre mi != null && menu != null && (@exists y: y.id == mi.id && menu.contains(y))
     * @post getSize = getSize()@pre - 1
     * @post (getElement ( i).getId() == id) ==> @forall k : [0 .. getSize() - 1] @
     * (k < i ==> getElement(k)@pre == getElement(k)) &&
     * (k > i ==> getElement(k + 1)@pre == getElement(k))
     */
    void deleteMenuItem(MenuItem mi);

    /**
     * @param mi menu item to add
     * @invariant isWellFormed()
     * @pre mi != null && menu != null && (@exists y: y.id == mi.id && menu.contains(y))
     * @post (getElement ( i).getId() == mi.getId()) ==> @forall k : [0 .. getSize() - 1] @
     * (k != i ==> getElement(k)@pre == getElement(k)) &&
     * (k == i ==> getElement(k)@pre == mi)
     */
    void editMenuItem(MenuItem mi);

    //Waiter: create new order; compute price for an order; generate bill in .txt format

    /**
     * @param order order to calculate price for
     * @return price as int
     * @invariant isWellFormed()
     * @pre orders != null && order != null && getSize() > 0 && (@exists y: y.id == order.id && orders.contains(y))
     * @post @nochange
     */
    public int computePrice(Order order);

    /**
     * @param order order for which to generate bill
     * @invariant isWellFormed()
     * @pre orders != null & order != null && getSize() > 0 && (@exists y: y.id == order.id && orders.contains(y))
     * @post @nochange
     */
    public void generateBill(Order order);

    /**
     * @param order order reference
     * @param list  menu items for given order
     * @invariant isWellFormed()
     * @pre order != null && orders != null && items != null && items.getSize() > 0
     * @post getSize() = getSize()@pre + 1
     */
    void createOrder(Order order, ArrayList<MenuItem> list);

}
