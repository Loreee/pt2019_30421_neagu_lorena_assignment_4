package DataLayer;

import BussinessLayer.Restaurant;

import java.io.*;

/**
 * @author Neagu Lorena
 */
public class RestaurantSerializator {

    public RestaurantSerializator() {

    }

    /**
     * Retrieves a restaurant instance from a file using deserialization
     *
     * @return restaurant instance
     * @throws IOException file not found
     */
    public static Restaurant citire() throws IOException {
        Restaurant r = new Restaurant();
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("orders.txt");
            ObjectInputStream in = new ObjectInputStream(fis);
            r = (Restaurant) in.readObject();
        } catch (FileNotFoundException e) {
            System.out.println("Fisierul nou ... ");
        } catch (Exception e) {
            System.out.println("Eroare la citirea datelor ... ");
            e.printStackTrace();
        } finally {
            if (fis != null)
                fis.close();
        }
        return r;
    }

    /**
     * Saves the state of a restaurant instance in a file using serialization
     *
     * @param r restaurant instance
     * @throws IOException file not found
     */
    public static void salvare(Restaurant r) throws IOException {
        FileOutputStream fos = new FileOutputStream("orders.txt");
        ObjectOutputStream out = new ObjectOutputStream(fos);
        out.writeObject(r);
    }

}
