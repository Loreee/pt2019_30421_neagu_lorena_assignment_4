package Start;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

import BussinessLayer.*;
import PresentationLayer.*;
import DataLayer.*;

public class Start {

	public static void main(String[] args) throws IOException {
		
		/*File file = new File("bill.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		PrintStream ps = new PrintStream(fos);
		System.setOut(ps);
		Restaurant restaurant = new Restaurant();

		ArrayList<MenuItem> baseProducts = new ArrayList<>();

        baseProducts.add(new BaseProduct(0, "salam", 10, 12.5));
        baseProducts.add(new BaseProduct(1, "branza", 10, 10));
        baseProducts.add(new BaseProduct(2, "paine", 10, 5.75));

        restaurant.createNewMenuItem(baseProducts.get(0));
        restaurant.createNewMenuItem(baseProducts.get(1));
        restaurant.createNewMenuItem(baseProducts.get(2));

        MenuItem sandwich = new CompositeProduct(0, "sandwich", 10, baseProducts);

        restaurant.createNewMenuItem(sandwich);

        Order order1 = new Order(0,1);
        restaurant.createOrder(order1, baseProducts);
        
     
		
        restaurant.generateBill(order1);
        
        ArrayList<MenuItem> sandwich_list = new ArrayList<>();
        Order order2 = new Order(1,2);
        sandwich_list.add(sandwich);
        restaurant.createOrder(order2, sandwich_list);
        restaurant.generateBill(order2);
		*/
        
		Restaurant restaurant = RestaurantSerializator.citire();
		View       view       = new View(restaurant);
        Controller controller = new Controller(view, restaurant);

        view.setVisible(true);
        
	}
}










/*   View view = new View("Restaurant Application");

view.updateMenuTable(view.getAdministratorPanel(), View.generateMenuTable(restaurant.getMenu()));
view.updateMenuTable(view.getWaiterPanel(), View.generateMenuTable(restaurant.getMenu()));
view.updateOrdersTable(view.getWaiterPanel(), View.generateOrderTable(restaurant.getOrders()));

// cooking table not working !!!

view.setVisible(true);
*/
/*    view.updateMenuTable(view.getAdministratorPanel(), View.generateMenuTable(restaurant.getMenu()));
view.updateMenuTable(view.getWaiterPanel(), View.generateMenuTable(restaurant.getMenu()));
view.updateOrdersTable(view.getWaiterPanel(), View.generateOrderTable(restaurant.getOrders()));
*/