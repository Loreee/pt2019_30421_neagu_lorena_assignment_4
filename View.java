package PresentationLayer;

import BussinessLayer.MenuItem;
import BussinessLayer.Order;
import BussinessLayer.Restaurant;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Defines the view part of the GUI
 *
 * @author Neagu Lorena
 */
public class View extends JFrame implements ItemListener {

    private JButton new_mi = new JButton("New Menu Item");
    private JButton new_composite_mi = new JButton("New Composite Menu Item");
    private JButton delete = new JButton("Delete Menu Item");
    private JButton edit = new JButton("Edit Menu Item");
    private JButton show = new JButton("View All Menu Items");
    private JButton save = new JButton("Save Data");
    private JButton cook = new JButton("What's cookin'?");

    //add
    private JTextField textid_mi = new JTextField(3);
    private JTextField textname = new JTextField(10);
    private JTextField textquantity = new JTextField(10);
    private JTextField textprice = new JTextField(10);


    private JLabel id_mi = new JLabel("Id");
    private JLabel name = new JLabel("Item Name");
    private JLabel quantity = new JLabel("Quantity");
    private JLabel price = new JLabel("Price");

    //delete
    private JTextField textdeletename = new JTextField(10);

    private JLabel deletename = new JLabel("Menu Item to be deleted");


    //edit
    private JTextField edittextid_mi = new JTextField(3);
    private JTextField edittextname = new JTextField(10);
    private JTextField edittextquantity = new JTextField(10);
    private JTextField edittextprice = new JTextField(10);

    private JLabel editid_mi = new JLabel("Id");
    private JLabel editname = new JLabel("Item Name");
    private JLabel editquantity = new JLabel("Quantity");
    private JLabel editprice = new JLabel("Price");


    //view all
    JPanel menuallcontent = new JPanel();

    //Waiter
    private JButton new_order = new JButton("New Order");
   // private JButton new_composite_order = new JButton("New Composite Order");
    private JButton show_order = new JButton("View All Orders");
    private JButton generate_bill = new JButton("Generate Bill");


    //add
    private JTextField textid_order = new JTextField(3);
    private JTextField texttable = new JTextField(10);
    private JTextField textorder = new JTextField(20);


    private JLabel id_order = new JLabel("Id");
    private JLabel table = new JLabel("Table");
    private JLabel order = new JLabel("Order");

    //generate bill
    private JLabel bill = new JLabel("Generate bill for order : ");
    private JTextField textbill = new JTextField(3);

    //view all
    JPanel orderallcontent = new JPanel();

    private JPanel cards = new JPanel(new CardLayout());
    final String MENUPANEL = "MENU";
    final String ADMINISTRATORPANEL = "Administrator";
    final String WAITERPANEL = "Waiter";
    final String CHEFPANEL = "Chef";
    TitledBorder border = new TitledBorder("RESTAURANT");
    private JLabel welcome = new JLabel("WELCOME!");

    Restaurant restaurant;

    /**
     * sets up the view of the GUI (the look and feel)
     *
     * @param r restaurant reference
     */
    public View(Restaurant r) {

        this.restaurant = r;
        this.setTitle("Restaurant");
        this.setPreferredSize(new Dimension(1000, 600));

        border.setTitleJustification(TitledBorder.CENTER);
        border.setTitlePosition(TitledBorder.CENTER);
        JPanel panel = new JPanel();
        JPanel emptycontent = new JPanel();
        emptycontent.setPreferredSize(new Dimension(0, 3));
        welcome.setFont(new Font("Courier", Font.BOLD, 45));
        border.setTitleFont(new Font("Calibri", Font.BOLD, 45));
        panel.setBorder(border);
        panel.add(emptycontent);
        panel.add(welcome, BorderLayout.CENTER);

        //administrator
        JPanel content1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
        content1.add(id_mi);
        content1.add(textid_mi);
        content1.add(name);
        content1.add(textname);
        content1.add(quantity);
        content1.add(textquantity);
        content1.add(price);
        content1.add(textprice);
        content1.add(new_mi);
        content1.add(new_composite_mi);
        JPanel deletecontent = new JPanel(new FlowLayout());
        deletecontent.add(deletename);
        deletecontent.add(textdeletename);
        deletecontent.add(delete);
        JPanel editcontent = new JPanel(new FlowLayout(FlowLayout.CENTER));
        editcontent.add(editid_mi);
        editcontent.add(edittextid_mi);
        editcontent.add(editname);
        editcontent.add(edittextname);
        editcontent.add(editquantity);
        editcontent.add(edittextquantity);
        editcontent.add(editprice);
        editcontent.add(edittextprice);
        editcontent.add(edit);
        menuallcontent.setLayout(new BoxLayout(menuallcontent, BoxLayout.PAGE_AXIS));
        menuallcontent.add(content1);
        menuallcontent.add(deletecontent);
        menuallcontent.add(editcontent);
        menuallcontent.add(show);
        menuallcontent.add(save);
        menuallcontent.setBorder(BorderFactory.createEmptyBorder(10, 10, 20, 10));

        //waiter
        JPanel contentorder = new JPanel(new FlowLayout(FlowLayout.CENTER));
        contentorder.add(id_order);
        contentorder.add(textid_order);
        contentorder.add(table);
        contentorder.add(texttable);
        contentorder.add(order);
        contentorder.add(textorder);
        contentorder.add(new_order);
       // contentorder.add(new_composite_order);
        JPanel contentbill = new JPanel(new FlowLayout(FlowLayout.CENTER));
        contentbill.add(bill);
        contentbill.add(textbill);
        contentbill.add(generate_bill);
        orderallcontent.setLayout(new BoxLayout(orderallcontent, BoxLayout.PAGE_AXIS));
        orderallcontent.add(contentorder);
        orderallcontent.add(contentbill);
        orderallcontent.add(show_order);
        orderallcontent.add(save);
        orderallcontent.setBorder(BorderFactory.createEmptyBorder(10, 10, 20, 10));

        JPanel comboBoxPane = new JPanel(new FlowLayout(FlowLayout.LEADING)); //use FlowLayout
        String comboBoxItems[] = {MENUPANEL, ADMINISTRATORPANEL, WAITERPANEL, CHEFPANEL};
        JComboBox cb = new JComboBox(comboBoxItems);
        cb.setEditable(false);
        cb.addItemListener(this);
        comboBoxPane.add(cb);

        
        JPanel chefcontent = new JPanel();
        chefcontent.add(cook);
        cards.add(panel, MENUPANEL);
        cards.add(menuallcontent, ADMINISTRATORPANEL);
        cards.add(orderallcontent, WAITERPANEL);
        cards.add(chefcontent, CHEFPANEL);

        this.getContentPane().add(comboBoxPane, BorderLayout.PAGE_START);
        this.getContentPane().add(cards, BorderLayout.CENTER);
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Creates a JTable for the restaurants' menu
     * @param list list of menu items
     */
    public void createMenuTable(ArrayList<MenuItem> list) {

    	if(list.isEmpty())
			try {
				throw new Exception("Nothing to see here");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				showError(e.getMessage());
			}
        int i = 0;

        Object[] columnNames = new Object[4];
        Object[][] rowData = new Object[list.size()][4];

        columnNames[0] = "id";
        columnNames[1] = "itemName";
        columnNames[2] = "quantity";
        columnNames[3] = "price";

        for (Iterator<MenuItem> it = list.iterator(); it.hasNext(); ) {
            MenuItem mi = it.next();
            rowData[i][0] = mi.getId();
            rowData[i][1] = mi.getItemName();
            rowData[i][2] = mi.getQuantity();
            rowData[i][3] = mi.computePrice();
            i++;
        }
        DefaultTableModel tableModel = new DefaultTableModel(rowData, columnNames);
        JTable table = new JTable(tableModel);
        tableModel.fireTableDataChanged();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.getColumnModel().getColumn(1).setPreferredWidth(150);
        table.getColumnModel().getColumn(2).setPreferredWidth(150);
        table.getColumnModel().getColumn(3).setPreferredWidth(150);
        JPanel p = new JPanel();
        p.add(table);
        p.add(table.getTableHeader());
        JFrame fr = new JFrame();

        fr.setTitle("Menu");

        fr.setPreferredSize(new Dimension(700, 500));

        fr.setContentPane(p);
        fr.setVisible(true);
        fr.pack();
    }
    public void createOrdersTable() {

        int i = 0;

        Object[] columnNames = new Object[4];
        Object[][] rowData = new Object[restaurant.getOrders().size()][4];

        columnNames[0] = "id";
        columnNames[1] = "Table";
        columnNames[2] = "Date";
        //columnNames[3] = "Contents";
        columnNames[3] = "Price";

        for (Map.Entry<Order, ArrayList<MenuItem>> mapEntry : restaurant.getOrders().entrySet()) {
            Order o = mapEntry.getKey();
            ArrayList<MenuItem> mi = mapEntry.getValue();
            rowData[i][0] = o.getId();
            rowData[i][1] = o.getTable();
            rowData[i][2] = o.getDate();
            rowData[i][3] = mi.get(0).computePrice();
            i++;
        }
        
        DefaultTableModel tableModel = new DefaultTableModel(rowData, columnNames);
        JTable table = new JTable(tableModel);
        tableModel.fireTableDataChanged();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        table.getColumnModel().getColumn(1).setPreferredWidth(150);
        table.getColumnModel().getColumn(2).setPreferredWidth(150);
        table.getColumnModel().getColumn(3).setPreferredWidth(150);
        JPanel p = new JPanel();
        p.add(table);
        p.add(table.getTableHeader());
        JFrame fr = new JFrame();

        fr.setTitle("Orders");

        fr.setPreferredSize(new Dimension(700, 500));

        fr.setContentPane(p);
        fr.setVisible(true);
        fr.pack();
    }
    /**
     * getters
     *
     * @return the values from a text field
     */
    public String getTextbill() {
        return textbill.getText();
    }

    public String getTextid_mi() {
        return textid_mi.getText();
    }

    public String getTextname() {
        return textname.getText();
    }

    public String getTextquantity() {
        return textquantity.getText();
    }

    public String getTextprice() {
        return textprice.getText();
    }

    public String getTextdeletename() {
        return textdeletename.getText();
    }

    public String getEdittextid_mi() {
        return edittextid_mi.getText();
    }

    public String getEdittextname() {
        return edittextname.getText();
    }

    public String getEdittextquantity() {
        return edittextquantity.getText();
    }

    public String getEdittextprice() {
        return edittextprice.getText();
    }

    public String getTextid_order() {
        return textid_order.getText();
    }

    public String getTexttable() {
        return texttable.getText();
    }

    public String getTextorder() {
        return textorder.getText();
    }

    /**
     * displays an error message on the View
     *
     * @param errMessage error message
     */
    void showError(String errMessage) {
        JOptionPane.showMessageDialog(this, errMessage);
    }

    /**
     * methods for adding listeners to each button of the GUI
     *
     * @param mal
     */
    void addNewMIListener(ActionListener mal) {
        new_mi.addActionListener(mal);
    }

    void addNewCompositeMIListener(ActionListener mal) {
        new_composite_mi.addActionListener(mal);
    }

    void addEditListener(ActionListener mal) {
        edit.addActionListener(mal);
    }

    void addDeleteListener(ActionListener mal) {
        delete.addActionListener(mal);
    }

    void addShowListener(ActionListener mal) {
        show.addActionListener(mal);
    }

    void addNewOrderListener(ActionListener mal) {
        new_order.addActionListener(mal);
    }

   /* void addNewCompositeOrderListener(ActionListener mal) {
        new_composite_order.addActionListener(mal);
    }*/

    void addShowOrderListener(ActionListener mal) {
        show_order.addActionListener(mal);
    }

    void addBillListener(ActionListener mal) {
        generate_bill.addActionListener(mal);
    }

    void addSaveListener(ActionListener mal) {
        save.addActionListener(mal);
    }
    void addCookListener(ActionListener mal) {
        cook.addActionListener(mal);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        // TODO Auto-generated method stub
        CardLayout cl = (CardLayout) (cards.getLayout());
        cl.show(cards, (String) e.getItem());

    }

}
